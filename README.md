# Indico - Room Booking Module

This guide describes Indico's room booking system. It aims at providing useful information to both regular users
in need of booking a room and space managers who need to configure their rooms in order to make them available to the
CERN community.

Indico users can book conference rooms for their conference and meeting purposes. Bookings can also be standalone - they
do not have to be bound to any event. Room managers can monitor and moderate all bookings. Space managers can create
rooms and manage their data, thanks to an Indico/Gesloc synchronization mechanism.

![](/img/main_room.png)
