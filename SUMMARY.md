# Summary

* [Introduction](README.md)

## For users
* [Getting around](room_booking/getting_around.md)
* [Booking Rooms](room_booking/booking_rooms.md)
* [Blocking Rooms](room_booking/blocking_rooms.md)

## For space managers
* [Room Configuration](room_booking/room_config.md)
* [Gesloc](room_booking/gesloc.md)

##
* [Glossary](room_booking/glossary.md)
