## Glossary

 * **Room** - meeting or conference room. Please note that the software is not suitable for managing other rooms, like offices, corridors, etc.

 * **Location** - physical location of rooms (e.g. CERN);
 * **Booking** - final reservation of a room. While considered final, it still may be rejected in case of emergency;
 * **Pre-booking** - unconfirmed reservation of a room. Pre-bookings are subject to acceptance or rejection by a manager/owner;
 * **Room Owner/Responsible** - person responsible for managing the room, which means
approving/rejecting any pre-bookings and cancelling any bookings/occurrences according to the needs;
 * **Management e-group** - Optional group of users who can execute the exact same functions as the room owner. I.e.
they co-manage the room;
 * **Room blocking** - a restriction created by a room manager which prevents users from booking the rooms listed in the blocking unless they have explicit permission to override the permission or own the rooms;
 * **Public rooms**  - rooms which can be booked by anyone, freely. They may or may not require approval of bookings. In
affirmative cases, the user will only be able to perform a "pre-booking" which has to be confirmed/rejected;
 * **Private rooms** - rooms that cannot be booked at all. Only the room owner or managers can book them.
