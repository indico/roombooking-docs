## Accessing the Room Booking Module

Just click on _Room Booking_ in the Indico home page, in the top dark bar. To book rooms for your event, go to the event management page and click on the _Room Booking_ option on the left banner. In both cases you will be asked to sign in. Use your Indico login.

---

### The First Page

The Room Booking Module first page depends on who is logged in. For most people, it shows the site _Map of Rooms_ that can be booked, as per the relevant menu option, on the left banner. The _Book a Room_ option, from the same menu, shows all the bookable rooms sorted by room number, together with the room capacity and equipment available. The _My Bookings_ menu option shows the bookings you have already done. Room managers can see bookings/blockings of rooms they manage by using the _My Rooms / Room Blocking_  options, always on the left banner.

---

### Room Names and filters

When you click on _Book a Room_ on the left banner, the entire list of rooms appears, arithmetically sorted.
Observe the _filter_ possibilities offered to you on the top row of the table. There you may type a building or room number, if you have a preferred location and restrict your selection with rooms with videoconferencing equipment and/or projector, required capacity and 'privacy' to facilitate your booking process.

The default name is built in the format: "building-floor-room \(location\)". If the room is know by a name, then it appears as: "building-floor-room - Common Name \(location\)".

Examples: "304-1-001 \(CERN\)", "4-3-004 - TH Discussion Room \(CERN\)"
![](/img/room_names_filters.png)

---

### Three Kinds of Room

There are three kinds of room. They determine how much freedom users have.

* _Public rooms_ which do not require confirmation \(black/green  
  colour\). This means all bookings are accepted.

* _Public rooms_ which do require confirmation \(orange colour\). You can  
  PRE-book them and wait for acceptance or rejection.

* _Private rooms_ which cannot be booked at all \(red colour\). Only the room manager can book his/her private room. If you need such a room, you should ask this person to create a booking for you.

In the image below you can see examples of these three types of rooms:
* _513-R-068_ is public, i.e. you try to book it, it is free at the time you want it, you have it!
* _513-1-024_ is public but requires confirmation, i.e. you try to book it, it is free at the time you want it, you PRE-book it, wait for moderator's approval, you have it!
* _513-R-070_ is private, i.e. you try to book it, you get the message: _You don't have rights to book this room_


![](/img/kinds_of_rooms.png)
