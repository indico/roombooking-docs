## Room Configuration

Most of the information below is synchronized from Gesloc, with a few exceptions (which are properly mentioned). If
you need to change that information, please have a look at the [Gesloc configuration](gesloc.md) section.
For attributes that are not modifiable through Gesloc, please contact the Service Desk.

### Basic attributes

 * **Room name**: (building-floor-number)
 * **Friendly name**
 * **"Where is the key?"**
 * **Capacity**
 * **Telephone**
 * **Area**

### Roles
 * **Room Owner/Responsible** - person responsible for managing the room, which means
approving/rejecting any pre-bookings and cancelling any bookings/occurrences according to the needs;
 * **Management e-group** - Optional group of users who can execute the exact same functions as the room owner. I.e.
they co-manage the room.

### Room behaviour

As explained in [Getting around](getting_around.md), rooms can be classified in
[three types](getting_around.md#three-kinds-of-room). The following properties define that behavior:

 * **Public** - defines whether the room is public or private. If not set, the room can only be reserved by its
owner/managers;
 * **Confirmations** - whether a booking in this room requires confirmation. If set, users (except for the room
owner/managers and Indico Admins) will be only able to create "Pre-bookings", which will then be confirmed/rejected by
the owner/manager.

Rooms can have **unavailability periods** which can be set directly in Indico. Specific **availability times** can also
be set for each room. Those two properties are not synchronized from Gesloc.

> **[info] Unavailabity periods**
>
> If one of your rooms is going to be unavailable due to e.g. refurbishment works, this is the best way to ensure users
> won't book it by mistake.

### Equipment

All information about the equipments contained in a room is retrieved from Gesloc, the exception being the specific
types of videoconference equipment available in the room (e.g. Vidyo or H323). If for some reason you need to set those
up, please contact the Service Desk.

### Other information not present in Gesloc

 * *IP Address* - the IP address of the computer located in this same room. Indico will allow any user connecting from
this computer to connect/disconnect video conferencing devices located therein;
 * *Notification email* - a list of e-mails that will be notified, in addition to the owner/managers each time there
is a new booking;
