## Booking rooms

### Single and multiple occurrence bookings

You may reserve a room more than _Once_, namely on a _Daily_, _Weekly_ or _Monthly_ basis. A common example is a weekly meeting \(which takes place at the same time every week\).

See, on the image below, once you've _selected_ your room and declared you want to book it _Weekly_, how _all Mondays_ appear darker than the other days on the calendar.

![](//img/book_frequency.png)
---

### Booking a Room

On the Indico main page, click the _Room Booking_ menu option. This is how you access the Room Booking Module. From there, click the _Book a Room_ menu option.

The booking process consists essentially on three steps:

* Specifying the search criteria
* Selecting an available period
* Confirming the reservation

![image169](/img/rb_booking_a_room.png)

In the first step you can specify the rooms, date range and time range in which you want to search for bookings.

* The _room picker_ allows you to filter by name, technical requirements, and minimum capacity. By clicking on a room you will mark it as a candidate for the booking.
* The _date range_ selector allows you to specify the frequency and flexibility of the booking. Frequency allows to book the room a single time, daily, weekly or monthly. Flexibility extends the search a few days before and after the specified date.
* The _time range_ simply serves the purpose of specifying the hours in which the room will be needed.

When you are done, click the button _Search_ to proceed to the next step.

![image170](/img/rb_conflicts.png)

In the second step, you will be presented with a calendar summarizing the rooms, dates and times you specified previously. In this view you can check all available periods \(marked in green\), other bookings \(in yellow\) or conflicts \(in red\).

This view is very versatile. Give it a try: set the frequency of the search to to _Weekly_, set the end date to be at least 2 months after the start date, and search again. See the room calendar in its full glory.

If you click on an available period with you will be redirected to the next step in which you will confirm the reservation.

![image171](/img/rb_booking_form.png)

In the third and final step, you will need to complete the booking form. Click _Book_ to confirm the reservation.

If your booking conflicts with others, you have two options:

* In general, it is better to resolve conflicts manually: by changing dates, hours or maybe trying another room.
* The alternative is to automatically book everything except conflicting days. This may be useful if you do long-term, recurring booking, for example, _whole year, every week_. Imagine everything looks perfect, except for several weeks when the room is not available. In this case check the 'skip conflicting dates' option to book everything except problematic dates.

If your form is correctly filled, the system will ensure your new booking does not conflict with others. When no conflicts are found, you will be shown the confirmation page which lists the details of your booking.

* Click _My bookings_ from the menu. Your new booking now appears on the list.

Note: some rooms require confirmation. These rooms are usually coded in orange. In this case, you cannot directly book them. You can only _PRE-book_ such a room. PRE-booking works exactly the same way as booking. The only difference is that you must wait for acceptance from the room manager.

Note: you can always modify your booking \(or PRE-booking\). It will again be checked for conflicts.

Note: you can always browse your own bookings and PRE-bookings using the menu options _My bookings_ and _My PRE-bookings_.

---

### View your Booking

You can view the bookings or pre-bookings that you made by going under the 'My bookings' and 'My PRE-bookings' sections of the Room Booking side menu. By selecting one of the bookings in the list, you will get the following :

![image172](/img/rb_booking_display.png)

This page contains all the information about a booking, and it allows you to modify, clone or cancel the booking. If the reservation is done for more than a single day, each of the occurences of the booking can be canceled individually. In addition, this page lets you watch the history of the booking \(i.e. all the past actions that were performed on this reservation\) if you are the creator of the booking. Only the most recent entry is partially displayed, but you can see the rest of it by clicking on the _Show All History..._ link. Additional information about a particular entry can be viewed by clicking the _More Info_ link.
